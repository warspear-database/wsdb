import Vue from 'vue'
import VueI18n from 'vue-i18n'
const req = require.context('./assets/langs', true, /\.json$/im)
const langs = req.keys().map(req)

Vue.use(VueI18n)

const messages = langs.reduce((a, c) => {
  a[c.code] = c.dictionary
  return a
}, {})

const i18n = new VueI18n({
  locale: 'en',
  messages,
  silentTranslationWarn: true
})

const available = langs.map((l) => {
  return {
    code: l.code,
    name: l.name
  }
})

export default { i18n, available }
