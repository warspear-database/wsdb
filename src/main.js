import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import lang from './lang'
import { Auth0Plugin } from '@/auth0'
import auth0Config from '@/assets/auth0.json'

Vue.config.productionTip = false

Vue.use(Auth0Plugin, {
  domain: auth0Config.domain,
  clientId: auth0Config.id,
  onRedirectCallback: (appState) => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    )
  }
})

new Vue({
  i18n: lang.i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
