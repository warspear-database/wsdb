import Vue from 'vue'
import VueRouter from 'vue-router'
import lang from '@/lang'
import news from '@/views/news'
import factions from '@/views/factions'
import faction from '@/views/faction'
import bonuses from '@/views/bonuses'
import bonus from '@/views/bonus'
import classes from '@/views/classes'
import clazz from '@/views/class'
import skills from '@/views/skills'
import skill from '@/views/skill'
import items from '@/views/items'
import item from '@/views/item'
import calculator from '@/views/calculator'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/' + getBrowserLang()
  },
  {
    path: '/:lang/:page(\\d+)?',
    name: 'news',
    component: news
  },
  {
    path: '/:lang/factions',
    name: 'factions',
    component: factions
  },
  {
    path: '/:lang/faction/:id(\\d+)',
    name: 'faction',
    component: faction
  },
  {
    path: '/:lang/bonuses',
    name: 'bonuses',
    component: bonuses
  },
  {
    path: '/:lang/bonus/:id(\\d+)',
    name: 'bonus',
    component: bonus
  },
  {
    path: '/:lang/classes',
    name: 'classes',
    component: classes
  },
  {
    path: '/:lang/class/:id(\\d+)',
    name: 'class',
    component: clazz
  },
  {
    path: '/:lang/skills',
    name: 'skills',
    component: skills
  },
  {
    path: '/:lang/skill/:id(\\d+)/:level(\\d+)?',
    name: 'skill',
    component: skill
  },
  {
    path: '/:lang/items',
    name: 'items_start',
    redirect: (to) => {
      return {
        name: 'items',
        params: {
          lang: to.params.lang,
          category: 0
        }
      }
    }
  },
  {
    path: '/:lang/items/:category(\\d+)',
    name: 'items',
    component: items
  },
  {
    path: '/:lang/item/:id(\\d+)/:mode(info|view|comments)',
    name: 'item',
    component: item
  },
  {
    path: '/:lang/calculator/run',
    name: 'calculator_run',
    redirect: (to) => {
      return {
        name: 'calculator',
        params: {
          lang: to.params.lang,
          id: 0,
          mode: 'build'
        }
      }
    }
  },
  {
    path: '/:lang/calculator/:id(\\d+)/:mode(build|result|comments)?',
    name: 'calculator',
    component: calculator
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

function getBrowserLang () {
  const browser = navigator.language.substr(0, 2)
  if (lang.available.some((l) => browser === l.code)) { return browser }
  return 'en'
}

router.beforeEach((to, from, next) => {
  if (!lang.available.some((l) => to.params.lang === l.code)) {
    to.params.lang = getBrowserLang()
    next({
      replace: true,
      name: to.name,
      params: to.params
    })
    return
  }
  lang.i18n.locale = to.params.lang
  next()
})

export default router
